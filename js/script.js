
function createListItem(array, argument = document.body) { 
     if (argument !== document.body) {
        argument = document.createElement(argument);
        document.body.prepend(argument);
     }

    array.slice().reverse().forEach(content => {
        let li = document.createElement("li");    
        li.textContent = content;
        argument.prepend(li);
    });
}

// Test
createListItem(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], 'ul');
createListItem(["1", "2", "3", "sea", "user", 23]);



